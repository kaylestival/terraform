
###### ADMIN DATA

variable "local_ip" {
  type = string
}



###### DROPLETS

variable "droplet_hostnames" {
  type = list(string)
}

variable "droplet_image" {
  type    = string
  default = "ubuntu-20-04-x64"
}

variable "droplet_region" {
  type    = string
  default = "nyc3"
}

variable "droplet_size" {
  type    = string
  default = "s-1vcpu-1gb"
}

variable "droplet_sshkeys" {
  type    = list(string)
  default = ["2a:e5:3a:0c:cc:3e:cd:c2:bc:2b:20:d9:a2:1a:53:0c"]
}

variable "private_key_path" {
  type    = string
  default = "./id_rsa"
}



###### LOAD BALANCER

variable "loadbalancer_name" {
  type    = string
  default = "do-lb-nyc3"
}



###### DATABASE

variable "database_cluster_name" {
  type    = string
  default = "do-clr-pg-nyc3"
}

variable "database_engine" {
  type    = string
  default = "pg"
}

variable "database_version" {
  type    = string
  default = "12"
}

variable "database_size" {
  type    = string
  default = "db-s-1vcpu-1gb"
}

variable "database_node_count" {
  type    = number
  default = 1
}

variable "database_name" {
  type    = string
  default = "do-dbpg-nyc3-01"
}

variable "database_username" {
  type    = string
  default = "pgsqluser"
}



###### FIREWALL

variable "firewall_name" {
  type = string
}

variable "libera_db" {
  type    = bool
  default = false
}


variable "libera_ssh" {
  type    = bool
  default = false
}