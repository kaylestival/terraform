####### ADMIN DATA
local_ip = "YOUR_PUBLIC_IP"
#token          = export DIGITALOCEAN_TOKEN


###### DROPLETS
droplet_region    = "nyc3"
droplet_hostnames = ["do-vm-nyc3-01", "do-vm-nyc3-02"]
private_key_path  = "./id_rsa"
#droplet_image     = ""
#droplet_size      = ""
#droplet_sshkeys   = ["",""]


###### LOAD BALANCER
loadbalancer_name = "do-lb-nyc3"


###### DATABASE
# https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/database_cluster
database_cluster_name = "do-clr-pg-nyc3"
database_name         = "do-dbpg-nyc3-01"
database_username     = "pgsqluser"
# database_engine      = "pg" # pg, mysql, redis or mongodb
# database_version     = "12"
# database_size        = "db-s-1vcpu-1gb"
# database_node_count  = 1


####### FIREWALL
firewall_name = "do-fw-nyc3-01"
libera_db     = false
libera_ssh    = false