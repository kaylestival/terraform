###### DROPLETS

output "droplets_hostnames" {
  value = digitalocean_droplet.web[*].name
}

output "droplets_public_ips" {
  value = digitalocean_droplet.web[*].ipv4_address
}



###### LOAD BALANCER

output "loadbalancer_public_ip" {
  value = digitalocean_loadbalancer.public.ip
}



###### DATABASE

output "database_cluster_hostname" {
  value = digitalocean_database_cluster.databasecluster.host
}

output "database_cluster_port" {
  value = digitalocean_database_cluster.databasecluster.port
}

output "database_cluster_uri" {
  value = nonsensitive(digitalocean_database_cluster.databasecluster.uri)
}

output "database_username" {
  value = digitalocean_database_user.dbnormaluser.name
}

output "database_username_password" {
  value = nonsensitive(digitalocean_database_user.dbnormaluser.password)
}