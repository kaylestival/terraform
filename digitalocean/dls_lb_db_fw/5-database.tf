###### DATABASE

resource "digitalocean_database_cluster" "databasecluster" {
  name       = var.database_cluster_name
  engine     = var.database_engine
  version    = var.database_version
  size       = var.database_size
  region     = var.droplet_region
  node_count = var.database_node_count
}

resource "digitalocean_database_db" "database" {
  cluster_id = digitalocean_database_cluster.databasecluster.id
  name       = var.database_name
}

resource "digitalocean_database_user" "dbnormaluser" {
  cluster_id = digitalocean_database_cluster.databasecluster.id
  name       = var.database_username
}