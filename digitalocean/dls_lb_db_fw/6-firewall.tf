###### FIREWALL

resource "digitalocean_database_firewall" "databasefirewall" {
  cluster_id = digitalocean_database_cluster.databasecluster.id

  rule {
    type  = "ip_addr"
    value = var.libera_db == true ? (var.local_ip) : ("127.0.0.1")
  }

  rule {
    type  = "tag"
    value = digitalocean_tag.dropletstag.name
  }
}

resource "digitalocean_firewall" "firewall" {
  name = var.firewall_name

  droplet_ids = digitalocean_droplet.web[*].id

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = var.libera_ssh == true ? [var.local_ip] : []
  }

  inbound_rule {
    protocol                  = "tcp"
    port_range                = "22"
    source_load_balancer_uids = [digitalocean_loadbalancer.public.id]
  }

  inbound_rule {
    protocol                  = "tcp"
    port_range                = "80"
    source_load_balancer_uids = [digitalocean_loadbalancer.public.id]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "udp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

}