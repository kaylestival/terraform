###### DROPLETS

output "droplets_hostnames" {
  value = digitalocean_droplet.web[*].name
}

output "droplets_public_ips" {
  value = digitalocean_droplet.web[*].ipv4_address
}



###### LOAD BALANCER

output "loadbalancer_public_ip" {
  value = digitalocean_loadbalancer.public.ip
}