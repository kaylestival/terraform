###### DROPLETS

resource "digitalocean_tag" "dropletstag" {
  name = "droplets"
}

resource "digitalocean_droplet" "web" {
  image    = var.droplet_image
  name     = var.droplet_hostnames[count.index]
  region   = var.droplet_region
  size     = var.droplet_size
  ssh_keys = var.droplet_sshkeys

  provisioner "remote-exec" {
    connection {
      type = "ssh"
      user = "root"
      host = self.ipv4_address
      #host        = digitalocean_droplet.web[count.index].ipv4_address
      private_key = file(pathexpand(var.private_key_path))
      timeout     = "6m"
    }
    inline = [
      "apt update",
      "apt upgrade -y",
      "apt-get update",
      "apt upgrade -y",
    ]
  }

  lifecycle {
    create_before_destroy = true
    ignore_changes        = [region]
  }

  count = length(var.droplet_hostnames)

  tags = [digitalocean_tag.dropletstag.id]

}