# **List of resource creation files**

**Cloud provider**: DigitalOcean 

- **dls directory**: Droplets Linux using an asymmetric key pair previasly created.
- **dls_lb directory**: Droplets Linux, load balancer and using an asymmetric key pair previasly created.
- **dls_lb_db_fw directory**: Droplets Linux, load balancer, database, firewall, and using an asymmetric key pair previasly created.

More, soon.
