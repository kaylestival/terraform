
###### ADMIN DATA

variable "local_ip" {
  type = string
}



###### DROPLETS

variable "droplet_hostnames" {
  type = list(string)
}

variable "droplet_image" {
  type    = string
  default = "ubuntu-20-04-x64"
}

variable "droplet_region" {
  type    = string
  default = "nyc3"
}

variable "droplet_size" {
  type    = string
  default = "s-1vcpu-1gb"
}

variable "droplet_sshkeys" {
  type    = list(string)
  default = ["2a:e5:3a:0c:cc:3e:cd:c2:bc:2b:20:d9:a2:1a:53:0c"]
}

variable "private_key_path" {
  type    = string
  default = "./id_rsa"
}