####### ADMIN DATA
local_ip = "YOUR_PUBLIC_IP"
#token          = export DIGITALOCEAN_TOKEN


###### DROPLETS
droplet_region    = "nyc3"
droplet_hostnames = ["do-vm-nyc3-01", "do-vm-nyc3-02"]
private_key_path  = "./id_rsa"
#droplet_image     = ""
#droplet_size      = ""
#droplet_sshkeys   = ["",""]