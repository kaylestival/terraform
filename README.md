# **Provisioning resources with Terraform**

This project organizes files for **resource provisioning** in the clouds using **Terraform** as IaC (Infrastructure as Code) tool:

- **Azure**
- **DigitalOcean**
- **AWS**
- **Google Cloud (GCP)**
- **Oracle Cloud (OCI)**

**Author**
- kaylestival@gmail.com
- [linkedin.com/in/kaylestival](linkedin.com/in/kaylestival)
