#!/bin/bash

/usr/bin/python3 -m venv py36env

source py36env/bin/activate

export FLASK_SECRET='394jmga9flsjc81njosn32jociq7loh4e9nqge8hkuaein8348sm'
export BUCKET_NAME='tcbvideobackupksv'

nohup /app/py36env/bin/gunicorn app:app --bind=0.0.0.0:8080 --workers=3 --timeout=30 --config=/app/config.py --access-logfile /app/access.log --error-logfile /app/error.log