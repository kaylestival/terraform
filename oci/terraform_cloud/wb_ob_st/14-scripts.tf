resource "null_resource" "remoteexec1" {
  
  depends_on = [
    oci_core_instance.webserver,
    oci_objectstorage_bucket.bucket_mod5
  ]

  connection {
    type        = "ssh"
    host        = oci_core_instance.webserver.public_ip
    user        = "opc"
    timeout     = "30m"
    private_key = var.ssh_private_key
  }

  provisioner "file" {
    source = "gunicorn.service"
    destination = "/tmp/gunicorn.service"
  }

  provisioner "file" {
    source = "start-server.sh"
    destination = "/tmp/start-server.sh"
  }


}

resource "null_resource" "remoteexec2" {

  depends_on = [
    oci_core_instance.webserver,
    oci_objectstorage_bucket.bucket_mod5,
    null_resource.remoteexec1
  ]

  connection {
    type        = "ssh"
    host        = oci_core_instance.webserver.public_ip
    user        = "opc"
    timeout     = "30m"
    private_key = var.ssh_private_key
  }

  provisioner "remote-exec" {
    inline = [
      "echo '1 - Criando regra no firewall:' && sudo firewall-cmd --zone=public --permanent --add-port=8080/tcp",
      "echo '2 - Fazendo reload das regras de firewall:' && sudo firewall-cmd --reload",
      "echo '3 - Criando diretório para logs do app:' && sudo mkdir /app && sudo chown opc:opc /app && cd /app",
      "echo '4 - Criando ambiente python:' && python3 -m venv py36env",
      "echo '5 - Recarregando arquivo de configuração:' && source py36env/bin/activate",
      "echo '6 - Baixando arquivo:' && wget https://objectstorage.us-ashburn-1.oraclecloud.com/p/V7oHPoleSVsmGaNcR7_mGEOrXtuHuhN7OsUJEF2rIY5EoKyNjz759bux1L4Ppn3b/n/idqfa2z2mift/b/bootcamp-oci/o/oci-f-handson-modulo-storage-appfiles.zip",
      "echo '7 - Descompactando arquivo baixado:' && unzip -o oci-f-handson-modulo-storage-appfiles.zip",
      "echo '8 - Fazendo upgrade do pip:' && pip install --upgrade pip",
      "echo '9 - Instalando requerimentos do python:' && pip install -r requirements.txt",
      "echo '10.1 - Ativando serviço gunicorn:' && sudo cp /tmp/gunicorn.service /etc/systemd/system/",
      "echo '10.2 - Ativando serviço gunicorn:' && sudo cp /tmp/start-server.sh /app/ && sudo chmod +x /app/start-server.sh",
      "echo '10.3 - Ativando serviço gunicorn:' && sudo systemctl enable gunicorn",
      "echo '11 - Iniciando serviço gunicorn:' && sudo systemctl daemon-reload && sudo systemctl restart gunicorn && sleep 20",
      "echo '12 - Verificando status do serviço gunicorn:' && sudo systemctl status gunicorn",
      "echo '13 - Aplicação pronta para ser acessada!!!'"
    ]
  }
}