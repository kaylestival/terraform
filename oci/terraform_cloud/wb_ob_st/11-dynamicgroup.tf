resource "oci_identity_dynamic_group" "dynamic_group_mod5" {
  #Required
  compartment_id = var.tenancy_ocid
  description    = "videobackup's site"
  matching_rule  = "tag.desenvolvimento.aplicacao.value='videobackup'"
  name           = "videobackup"
  #Optional
  freeform_tags = var.general_tags
}