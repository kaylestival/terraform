# PROVIDER OCI - VARIABLES ON THE TERRAFORM CLOUD'S WORKSPACE
variable "tenancy_ocid" {}
variable "user_ocid" {}
variable "fingerprint" {}
variable "private_key" {}
variable "region" {}
variable "ssh_private_key" {}
variable "ssh_public_key" {}
variable "compartment_network_id" {}
variable "compartment_compute_id" {}
variable "compartment_storage_id" {}

# GENERAL
variable "availability_domain_number" {
  default = 1
}
variable "general_tags" {
  type = map(string)
  default = {
    "Created_by" = "KayleStival"
    "Year"       = "2021"
    "Source"     = "TheCloudBootcamp"
    "Course"     = "BootcampOCI"
    "Module"     = "5"
    "Year"       = "2021"
  }
}

# INTERNET GATEWAY
variable "internet_gateway_enabled" {
  type    = bool
  default = true
}
variable "internet_gateway_display_name" {
  default = "int-gw-mod05"
}

# DEFAULT ROUTE TABLE
variable "route_table_route_rules_description" {
  default = "Route internal connections to the Internet"
}
variable "route_table_route_rules_destination" {
  default = "0.0.0.0/0"
}

# VCN
variable "vcn_display_name" {
  default = "vcn-mod05"
}
variable "vcn_dns_label" {
  default = "vcnmod05"
}

# SECURITY LIST 01
variable "securitylist1_display_name" {
  default = "sl-mod05"
}

# SUBNET 01
variable "subnet1_cidr_block" {
  default = "10.0.1.0/24"
}
variable "subnet1_display_name" {
  default = "subnet-mod05"
}
variable "subnet1_dns_label" {
  default = "subnetmod05"
}
variable "subnet1_prohibit_internet_ingress" {
  type    = bool
  default = false
}
variable "subnet1_prohibit_public_ip_on_vnic" {
  type    = bool
  default = false
}

# FREE IMAGE
variable "image1_instance_so" {
  default = "Oracle Linux"
}
variable "image1_instance_so_version" {
  default = "7.9"
}
variable "image1_instance_shape" {
  default = "VM.Standard2.1"
}

# WEBSERVER'S INSTANCE
variable "instance1_shape" {
  default = "VM.Standard.E2.1.Micro"
}
variable "instance1_display_name" {
  default = "webserver"
}
variable "instance1_fault_domain" {
  default = "FAULT-DOMAIN-1"
}
variable "instance1_is_pv_encryption_in_transit_enabled" {
  type    = bool
  default = true
}
variable "instance1_shape_config_memory_in_gbs" {
  default = 1
}
variable "instance1_shape_config_ocpus" {
  default = 1
}
variable "instance1_source_type" {
  default = "image"
}
variable "instance1_boot_volume_size" {
  default = "100"
}
# VNIC
variable "instance1_create_vnic_details_assign_private_dns_record" {
  type    = bool
  default = true
}

# TAG NAMESPACE
variable "tag_namespace_description" {
  default = "Module 05 - The Cloud Bootcamp"
}
variable "tag_namespace_name" {
  default = "desenvolvimento"
}
variable "tag_description" {
  default = "aplicacao"
}
variable "tag_name" {
  default = "aplicacao"
}