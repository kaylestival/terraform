resource "oci_core_default_route_table" "routetable" {
  manage_default_resource_id = oci_core_vcn.vcn.default_route_table_id
  #Required
  compartment_id = var.compartment_network_id
  #Optional
  freeform_tags = var.general_tags
  route_rules {
    #Required
    network_entity_id = oci_core_internet_gateway.internetgateway.id
    #Optional
    description = var.route_table_route_rules_description
    destination = var.route_table_route_rules_destination
  }
}