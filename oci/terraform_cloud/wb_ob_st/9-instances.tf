data "oci_identity_availability_domain" "ad" {
  compartment_id = var.tenancy_ocid
  ad_number      = var.availability_domain_number
}

data "oci_core_images" "freeimage" {
  compartment_id           = var.compartment_compute_id
  operating_system         = var.image1_instance_so
  operating_system_version = var.image1_instance_so_version
  shape                    = var.image1_instance_shape
}

resource "oci_core_instance" "webserver" {
  #Necessary
  depends_on = [
    oci_core_subnet.subnet
  ]
  #Required
  availability_domain = data.oci_identity_availability_domain.ad.name
  compartment_id      = var.compartment_compute_id
  shape               = var.instance1_shape
  #Optional
  create_vnic_details {
    #Optional
    freeform_tags             = var.general_tags
    subnet_id                 = oci_core_subnet.subnet.id
    assign_private_dns_record = var.instance1_create_vnic_details_assign_private_dns_record
  }
  display_name                        = var.instance1_display_name
  fault_domain                        = var.instance1_fault_domain
  freeform_tags                       = var.general_tags
  defined_tags                        = {"${oci_identity_tag_namespace.tag_namespace_mod5.name}.${oci_identity_tag.tag_mod5.name}" = "videobackup"}
  is_pv_encryption_in_transit_enabled = var.instance1_is_pv_encryption_in_transit_enabled
  metadata = {
    ssh_authorized_keys = var.ssh_public_key
  }
  shape_config {
    #Optional
    memory_in_gbs = var.instance1_shape_config_memory_in_gbs
    ocpus         = var.instance1_shape_config_ocpus
  }
  source_details {
    #Required
    source_id   = data.oci_core_images.freeimage.images.0.id
    source_type = var.instance1_source_type
    #Optional
    boot_volume_size_in_gbs = var.instance1_boot_volume_size
  }
  preserve_boot_volume = false
}