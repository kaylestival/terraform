output "webserver_public_ip" {
  description = "Public IPs of created instances."
  value       = oci_core_instance.webserver.public_ip
}