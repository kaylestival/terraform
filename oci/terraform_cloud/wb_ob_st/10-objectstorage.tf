data "oci_objectstorage_namespace" "os_namespace_mod5" {
  compartment_id = var.compartment_storage_id
}

resource "oci_objectstorage_bucket" "bucket_mod5" {
  compartment_id = var.compartment_storage_id
  name           = "tcbvideobackupksv"
  namespace      = data.oci_objectstorage_namespace.os_namespace_mod5.namespace
  access_type    = "NoPublicAccess"
  freeform_tags  = var.general_tags
}