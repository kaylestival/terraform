resource "oci_core_security_list" "slsubnet" {
  compartment_id = var.compartment_network_id
  vcn_id         = oci_core_vcn.vcn.id
  display_name   = var.securitylist1_display_name
  # egress for Internet access
  egress_security_rules {
    destination = "0.0.0.0/0"
    protocol    = "all"
    description = "Internet access for VM's with this security list"
  }
  # ingress for ssh access
  ingress_security_rules {
    protocol    = "6"
    source      = "0.0.0.0/0"
    description = "SSH connection"
    tcp_options {
      max = "22"
      min = "22"
    }
  }
  # ingress for http access
  ingress_security_rules {
    protocol    = "6"
    source      = "0.0.0.0/0"
    description = "HTTP connection"
    tcp_options {
      max = "8080"
      min = "8080"
    }
  }
  # ingress for ICMP protocol (DON'T REMOVE)
  ingress_security_rules {
    protocol    = "1"
    source      = "0.0.0.0/0"
    description = "ICMP traffic for: 3, 4 Destination Unreachable: Fragmentation Needed and Don't Fragment was Set"
    icmp_options {
      type = "3"
      code = "4"
    }
  }
  # ingress for ICMP protocol (DON'T REMOVE)
  ingress_security_rules {
    protocol    = "1"
    source      = "10.0.0.0/16"
    description = "ICMP traffic for: 3 Destination Unreachable"
    icmp_options {
      type = "3"
    }
  }
  freeform_tags = var.general_tags
}