
data "oci_identity_compartment" "compartment_storage" {
  id = var.compartment_storage_id
}

resource "oci_identity_policy" "policy_mod5" {
  depends_on = [
    oci_identity_dynamic_group.dynamic_group_mod5
  ]
  compartment_id = var.compartment_storage_id
  description    = "videobackup's site manager"
  name           = "VideoBackupPolicy"
  statements     = ["Allow dynamic-group ${oci_identity_dynamic_group.dynamic_group_mod5.name} to manage objects in compartment ${data.oci_identity_compartment.compartment_storage.name}"]
  freeform_tags  = var.general_tags
}