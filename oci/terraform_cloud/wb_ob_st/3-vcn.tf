resource "oci_core_vcn" "vcn" {
  # Required
  compartment_id = var.compartment_network_id
  # Optional
  cidr_blocks   = ["10.0.0.0/16"]
  display_name  = var.vcn_display_name
  dns_label     = var.vcn_dns_label
  freeform_tags = var.general_tags
}