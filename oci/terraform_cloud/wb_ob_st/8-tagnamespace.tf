resource "oci_identity_tag_namespace" "tag_namespace_mod5" {
  compartment_id = var.compartment_storage_id
  description    = var.tag_namespace_description
  name           = var.tag_namespace_name
}

resource "oci_identity_tag" "tag_mod5" {
  description      = var.tag_description
  name             = var.tag_name
  tag_namespace_id = oci_identity_tag_namespace.tag_namespace_mod5.id
}