# PROVIDER OCI
variable "tenancy_ocid" {}
variable "user_ocid" {}
variable "fingerprint" {}
variable "private_key_path" {}
variable "region" {}

# GENERAL
variable "availability_domain_number" {}
variable "compartment_db_id" {}
variable "general_tags" {
  type = map(string)
  default = {
    "Created_by" = "KayleStival"
    "Year"       = "2021"
  }
}

# AUTONOMOUS DATABASE
variable "autonomous_database_db_name" {}
variable "autonomous_database_display_name" {}
variable "autonomous_database_db_workload" {}
variable "autonomous_database_is_dedicated" {}
variable "autonomous_database_is_free_tier" {}
variable "autonomous_database_db_version" {}
variable "autonomous_database_customer_contacts_email" {}
variable "autonomous_database_license_model" {}
variable "autonomous_database_cpu_core_count" {}
variable "autonomous_database_data_storage_size_in_tbs" {}
variable "autonomous_database_is_auto_scaling_enabled" {}
variable "autonomous_database_is_preview_version_with_service_terms_accepted" {}

# SENDER EMAIL
variable "sender_email_address" {}

# SMTP CREDENTIAL
variable "smtp_credential_description" {}