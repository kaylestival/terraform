resource "oci_identity_smtp_credential" "smtp_credential" {
  description = var.smtp_credential_description
  user_id     = var.user_ocid
}