output "autonomous_database_admin_password" {
  value = random_string.autonomous_database_admin_password.result
}

output "autonomous_database_high_connection_string" {
  value = lookup(oci_database_autonomous_database.autonomous_database.connection_strings.0.all_connection_strings, "HIGH")
}

output "smtp_credential_username" {
  value = oci_identity_smtp_credential.smtp_credential.username
}

output "smtp_credential_password" {
  value = oci_identity_smtp_credential.smtp_credential.password
}