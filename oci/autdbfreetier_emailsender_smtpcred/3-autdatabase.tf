resource "random_string" "autonomous_database_admin_password" {
  length      = 16
  min_numeric = 1
  min_lower   = 1
  min_upper   = 1
  min_special = 1
}

resource "oci_database_autonomous_database" "autonomous_database" {
  compartment_id                                 = var.compartment_db_id
  db_name                                        = var.autonomous_database_db_name
  admin_password                                 = random_string.autonomous_database_admin_password.result
  display_name                                   = var.autonomous_database_display_name
  freeform_tags                                  = var.general_tags
  db_workload                                    = var.autonomous_database_db_workload
  db_version                                     = var.autonomous_database_db_version
  is_dedicated                                   = var.autonomous_database_is_dedicated
  is_free_tier                                   = var.autonomous_database_is_free_tier
  license_model                                  = var.autonomous_database_license_model
  cpu_core_count                                 = var.autonomous_database_cpu_core_count
  data_storage_size_in_tbs                       = var.autonomous_database_data_storage_size_in_tbs
  is_auto_scaling_enabled                        = var.autonomous_database_is_auto_scaling_enabled
  is_preview_version_with_service_terms_accepted = var.autonomous_database_is_preview_version_with_service_terms_accepted
  customer_contacts {
    email = var.autonomous_database_customer_contacts_email
  }
}