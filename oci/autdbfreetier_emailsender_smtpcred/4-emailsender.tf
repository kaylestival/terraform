resource "oci_email_sender" "email_sender" {
  compartment_id = var.compartment_db_id
  email_address  = var.sender_email_address
  freeform_tags  = var.general_tags
}