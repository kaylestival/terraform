resource "null_resource" "remoteexecfin" {
  depends_on = [oci_core_instance.vmfinanceiro]
  connection {
    type        = "ssh"
    host        = oci_core_instance.vmfinanceiro.public_ip
    user        = "opc"
    timeout     = "30m"
    private_key = file(var.instance1_ssh_private_key_file)
  }
  provisioner "remote-exec" {
    inline = [
      "sudo yum update -y",
      "sudo yum install httpd -y",
      "sudo systemctl start httpd",
      "sudo systemctl enable httpd",
      "sudo firewall-cmd --zone=public --add-service=http",
      "sudo firewall-cmd --permanent --zone=public --add-service=http",
      "cd /var/www/html",
      "sudo wget https://objectstorage.us-ashburn-1.oraclecloud.com/p/8pVpr-wTqCWT7xuMdxWu9z5pSD5UjCYpXFaInFrhW3tmoySyD2eR2Foz6ksQWm5y/n/idqfa2z2mift/b/bootcamp-oci/o/oci-f-handson-modulo-networking-website-files.zip",
      "sudo unzip oci-f-handson-modulo-networking-website-files.zip",
      "sudo chown apache:apache *.*",
      "sudo rm -rf __MACOSX oci-f-handson-modulo-networking-website-files.zip"
    ]
  }
}

resource "null_resource" "remoteexeccom" {
  depends_on = [oci_core_instance.vmcomercial]
  connection {
    type        = "ssh"
    host        = oci_core_instance.vmcomercial.public_ip
    user        = "opc"
    timeout     = "30m"
    private_key = file(var.instance1_ssh_private_key_file)
  }
  provisioner "remote-exec" {
    inline = [
      "sudo yum update -y",
      "sudo yum install sed -y",
      "sudo  yum -y groups install \"Server with GUI\"",
      "sudo yum group list",
      "sudo yum install tigervnc-server -y",
      "cd /lib/systemd/system/",
      "sudo cp vncserver@.service /etc/systemd/system/vncserver@:1.service",
      "sudo sed -i 's/wrapper <USER>/wrapper opc/' /etc/systemd/system/vncserver@:1.service"
    ]
  }
}