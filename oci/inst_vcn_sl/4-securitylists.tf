resource "oci_core_security_list" "slsubnetfin" {
  #Required
  compartment_id = var.compartment_network_id
  vcn_id         = oci_core_vcn.vcn_mod3.id
  #Optional
  display_name = var.securitylist1_display_name
  # egress for Internet access
  egress_security_rules {
    #Required
    destination = var.securitylist1_egress_security_rules_destination
    protocol    = var.securitylist1_egress_security_rules_protocol
    #Optional
    description = var.securitylist1_egress_security_rules_description
    #    stateless   = var.security_list_egress_security_rules_stateless
  }
  # ingress for ssh access
  ingress_security_rules {
    #Required
    protocol = var.securitylist1_ingress1_security_rules_protocol
    source   = var.securitylist1_ingress1_security_rules_source
    #Optional
    description = var.securitylist1_ingress1_security_rules_description
    #    stateless   = var.security_list_ingress_security_rules_stateless
    tcp_options {
      #Optional
      max = var.securitylist1_ingress1_security_rules_tcp_options_destination_port_range_max
      min = var.securitylist1_ingress1_security_rules_tcp_options_destination_port_range_min
    }
  }
  # ingress for http access
  ingress_security_rules {
    #Required
    protocol = var.securitylist1_ingress2_security_rules_protocol
    source   = var.securitylist1_ingress2_security_rules_source
    #Optional
    description = var.securitylist1_ingress2_security_rules_description
    #    stateless   = var.security_list_ingress_security_rules_stateless
    tcp_options {
      #Optional
      max = var.securitylist1_ingress2_security_rules_tcp_options_destination_port_range_max
      min = var.securitylist1_ingress2_security_rules_tcp_options_destination_port_range_min
    }
  }
  # ingress for ICMP protocol (DON'T REMOVE)
  ingress_security_rules {
    #Required
    protocol = var.securitylist_ingress_security_rules_protocol
    source   = var.securitylist_ingress_security_rules_source1
    #Optional
    description = var.securitylist_ingress_security_rules_description1
    #    stateless   = var.security_list_ingress_security_rules_stateless
    icmp_options {
      #Required
      type = var.securitylist_ingress_security_rules_icmp_options_type
      #Optional
      code = var.securitylist_ingress_security_rules_icmp_options_code
    }
  }
  # ingress for ICMP protocol (DON'T REMOVE)
  ingress_security_rules {
    #Required
    protocol = var.securitylist_ingress_security_rules_protocol
    source   = var.securitylist_ingress_security_rules_source2
    #Optional
    description = var.securitylist_ingress_security_rules_description2
    #    stateless   = var.security_list_ingress_security_rules_stateless
    icmp_options {
      #Required
      type = var.securitylist_ingress_security_rules_icmp_options_type
    }
  }
  freeform_tags = var.general_tags
}


resource "oci_core_security_list" "slsubnetcom" {
  #Required
  compartment_id = var.compartment_network_id
  vcn_id         = oci_core_vcn.vcn_mod3.id
  #Optional
  display_name = var.securitylist2_display_name
  # egress for Internet access
  egress_security_rules {
    #Required
    destination = var.securitylist2_egress_security_rules_destination
    protocol    = var.securitylist2_egress_security_rules_protocol
    #Optional
    description = var.securitylist2_egress_security_rules_description
    #    stateless   = var.security_list_egress_security_rules_stateless
  }
  # ingress for http access
  ingress_security_rules {
    #Required
    protocol = var.securitylist2_ingress1_security_rules_protocol
    source   = var.securitylist2_ingress1_security_rules_source
    #Optional
    description = var.securitylist2_ingress1_security_rules_description
    #    stateless   = var.security_list_ingress_security_rules_stateless
    tcp_options {
      #Optional
      max = var.securitylist2_ingress1_security_rules_tcp_options_destination_port_range_max
      min = var.securitylist2_ingress1_security_rules_tcp_options_destination_port_range_min
    }
  }
  # ingress for ICMP protocol (DON'T REMOVE)
  ingress_security_rules {
    #Required
    protocol = var.securitylist_ingress_security_rules_protocol
    source   = var.securitylist_ingress_security_rules_source1
    #Optional
    description = var.securitylist_ingress_security_rules_description1
    #    stateless   = var.security_list_ingress_security_rules_stateless
    icmp_options {
      #Required
      type = var.securitylist_ingress_security_rules_icmp_options_type
      #Optional
      code = var.securitylist_ingress_security_rules_icmp_options_code
    }
  }
  # ingress for ICMP protocol (DON'T REMOVE)
  ingress_security_rules {
    #Required
    protocol = var.securitylist_ingress_security_rules_protocol
    source   = var.securitylist_ingress_security_rules_source2
    #Optional
    description = var.securitylist_ingress_security_rules_description2
    #    stateless   = var.security_list_ingress_security_rules_stateless
    icmp_options {
      #Required
      type = var.securitylist_ingress_security_rules_icmp_options_type
    }
  }
  freeform_tags = var.general_tags
}