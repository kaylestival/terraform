resource "oci_core_subnet" "subnetfin" {
  #Necessary
  depends_on = [
    oci_core_vcn.vcn_mod3,
    oci_core_security_list.slsubnetfin
  ]
  #Required
  cidr_block     = var.subnet1_cidr_block
  compartment_id = var.compartment_network_id
  vcn_id         = oci_core_vcn.vcn_mod3.id
  #Optional
  availability_domain        = data.oci_identity_availability_domain.ad.name
  display_name               = var.subnet1_display_name
  dns_label                  = var.subnet1_dns_label
  prohibit_internet_ingress  = var.subnet1_prohibit_internet_ingress
  prohibit_public_ip_on_vnic = var.subnet1_prohibit_public_ip_on_vnic
  security_list_ids          = [oci_core_security_list.slsubnetfin.id]
  freeform_tags              = var.general_tags
}

resource "oci_core_subnet" "subnetcom" {
  #Necessary
  depends_on = [
    oci_core_vcn.vcn_mod3,
    oci_core_security_list.slsubnetcom
  ]
  #Required
  cidr_block     = var.subnet2_cidr_block
  compartment_id = var.compartment_network_id
  vcn_id         = oci_core_vcn.vcn_mod3.id
  #Optional
  availability_domain        = data.oci_identity_availability_domain.ad.name
  display_name               = var.subnet2_display_name
  dns_label                  = var.subnet2_dns_label
  prohibit_internet_ingress  = var.subnet2_prohibit_internet_ingress
  prohibit_public_ip_on_vnic = var.subnet2_prohibit_public_ip_on_vnic
  security_list_ids          = [oci_core_security_list.slsubnetcom.id]
  freeform_tags              = var.general_tags
}