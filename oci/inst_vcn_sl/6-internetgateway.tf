resource "oci_core_internet_gateway" "internetgateway" {
  #Required
  compartment_id = var.compartment_network_id
  vcn_id         = oci_core_vcn.vcn_mod3.id
  #Optional
  enabled       = var.internet_gateway_enabled
  display_name  = var.internet_gateway_display_name
  freeform_tags = var.general_tags
}