output "vmfin_public_ip" {
  description = "Public IPs of created instances. "
  value       = oci_core_instance.vmfinanceiro.public_ip
}

#output "vmcom_public_ip" {
#  description = "Public IPs of created instances. "
#  value       = oci_core_instance.vmcomercial.public_ip
#}