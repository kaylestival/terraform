# PROVIDER OCI
variable "tenancy_ocid" {}
variable "user_ocid" {}
variable "fingerprint" {}
variable "private_key_path" {}
variable "private_key_password" {}
variable "region" {}

# GENERAL
variable "availability_domain_number" {}
variable "compartment_network_id" {}
variable "compartment_compute_id" {}
variable "general_tags" {
  type = map(string)
  default = {
    "Created_by" = "KayleStival"
    "Year"       = "2021"
  }
}

# INTERNET GATEWAY
variable "internet_gateway_enabled" {
  type    = bool
  default = false
}
variable "internet_gateway_display_name" {
  type    = string
  default = "ing-gw"
}

# DEFAULT ROUTE TABLE
variable "route_table_display_name" {}
variable "route_table_route_rules_description" {
  type    = string
  default = "Route internal connections to the Internet"
}
variable "route_table_route_rules_destination" {
  type    = string
  default = "0.0.0.0/0"
}

# VCN
variable "vcn_cidr_blocks" {}
variable "vcn_display_name" {}
variable "vcn_dns_label" {}

# DEFAULT SECURITY LIST INGRESS FOR ICMP PROTOCOL (DON'T CHANGE)
variable "securitylist_ingress_security_rules_protocol" {
  type    = string
  default = "1"
}
variable "securitylist_ingress_security_rules_source1" {
  type    = string
  default = "0.0.0.0/0"
}
variable "securitylist_ingress_security_rules_source2" {
  type    = string
  default = "10.0.0.0/16"
}
variable "securitylist_ingress_security_rules_description1" {
  type    = string
  default = "ICMP traffic for: 3, 4 Destination Unreachable: Fragmentation Needed and Don't Fragment was Set"
}
variable "securitylist_ingress_security_rules_description2" {
  type    = string
  default = "ICMP traffic for: 3 Destination Unreachable"
}
variable "securitylist_ingress_security_rules_icmp_options_type" {
  type    = string
  default = "3"
}
variable "securitylist_ingress_security_rules_icmp_options_code" {
  type    = string
  default = "4"
}

# SECURITY LIST 01 (FINANCEIRO)
variable "securitylist1_display_name" {}
# egress for Internet access
variable "securitylist1_egress_security_rules_destination" {
  type    = string
  default = "0.0.0.0/0"
}
variable "securitylist1_egress_security_rules_protocol" {
  type    = string
  default = "all"
}
variable "securitylist1_egress_security_rules_description" {
  type    = string
  default = "Internet access for VM's with this security list"
}
# ingress for ssh access
variable "securitylist1_ingress1_security_rules_protocol" {}
variable "securitylist1_ingress1_security_rules_source" {}
variable "securitylist1_ingress1_security_rules_description" {}
variable "securitylist1_ingress1_security_rules_tcp_options_destination_port_range_max" {}
variable "securitylist1_ingress1_security_rules_tcp_options_destination_port_range_min" {}
# ingress for http access
variable "securitylist1_ingress2_security_rules_protocol" {}
variable "securitylist1_ingress2_security_rules_source" {}
variable "securitylist1_ingress2_security_rules_description" {}
variable "securitylist1_ingress2_security_rules_tcp_options_destination_port_range_max" {}
variable "securitylist1_ingress2_security_rules_tcp_options_destination_port_range_min" {}

# SECURITY LIST 02 (COMERCIAL)
variable "securitylist2_display_name" {}
# egress for Internet access
variable "securitylist2_egress_security_rules_destination" {
  type    = string
  default = "0.0.0.0/0"
}
variable "securitylist2_egress_security_rules_protocol" {
  type    = string
  default = "all"
}
variable "securitylist2_egress_security_rules_description" {
  type    = string
  default = "Internet access for VM's with this security list"
}
# ingress for ssh access
variable "securitylist2_ingress1_security_rules_protocol" {}
variable "securitylist2_ingress1_security_rules_source" {}
variable "securitylist2_ingress1_security_rules_description" {}
variable "securitylist2_ingress1_security_rules_tcp_options_destination_port_range_max" {}
variable "securitylist2_ingress1_security_rules_tcp_options_destination_port_range_min" {}

# SUBNET 01 (FINANCEIRO)
variable "subnet1_cidr_block" {}
variable "subnet1_display_name" {}
variable "subnet1_dns_label" {}
variable "subnet1_prohibit_internet_ingress" {}
variable "subnet1_prohibit_public_ip_on_vnic" {}

# SUBNET 02 (COMERCIAL)
variable "subnet2_cidr_block" {}
variable "subnet2_display_name" {}
variable "subnet2_dns_label" {}
variable "subnet2_prohibit_internet_ingress" {}
variable "subnet2_prohibit_public_ip_on_vnic" {}

# FREE IMAGE
variable "image1_instance_so" {
  type    = string
  default = "Oracle Linux"
}
variable "image1_instance_so_version" {
  type    = string
  default = "7.9"
}
variable "image1_instance_shape" {
  type    = string
  default = "VM.Standard.E2.1.Micro"
}

# FINANCEIRO'S INSTANCE
variable "instance1_shape" {
  type    = string
  default = "VM.Standard2.1"
}
variable "instance1_display_name" {
  type    = string
  default = "vm01-temp"
}
variable "instance1_fault_domain" {
  type    = string
  default = "FAULT-DOMAIN-1"
}
variable "instance1_is_pv_encryption_in_transit_enabled" {
  type    = bool
  default = true
}
variable "instance1_ssh_public_key_file" {
  type    = string
  default = "~/.ssh/id_rsa.pub"
}
variable "instance1_ssh_private_key_file" {
  type    = string
  default = "~/.ssh/id_rsa"
}
variable "instance1_shape_config_memory_in_gbs" {}
variable "instance1_shape_config_ocpus" {}
variable "instance1_source_type" {
  type    = string
  default = "image"
}
variable "instance1_boot_volume_size" {
  type    = string
  default = "100"
}
# VNIC
variable "instance1_create_vnic_details_assign_private_dns_record" {
  type    = bool
  default = true
}
variable "instance1_create_vnic_details_assign_public_ip" {
  type    = bool
  default = false
}

# COMERCIAL'S INSTANCE
variable "instance2_shape" {
  type    = string
  default = "VM.Standard2.1"
}
variable "instance2_display_name" {
  type    = string
  default = "vm01-temp"
}
variable "instance2_fault_domain" {
  type    = string
  default = "FAULT-DOMAIN-1"
}
variable "instance2_is_pv_encryption_in_transit_enabled" {
  type    = bool
  default = true
}
variable "instance2_ssh_public_key_file" {
  type    = string
  default = "~/.ssh/id_rsa.pub"
}
variable "instance2_ssh_private_key_file" {
  type    = string
  default = "~/.ssh/id_rsa"
}
variable "instance2_shape_config_memory_in_gbs" {}
variable "instance2_shape_config_ocpus" {}
variable "instance2_source_type" {
  type    = string
  default = "image"
}
variable "instance2_boot_volume_size" {
  type    = string
  default = "100"
}
# VNIC
variable "instance2_create_vnic_details_assign_private_dns_record" {
  type    = bool
  default = true
}
variable "instance2_create_vnic_details_assign_public_ip" {
  type    = bool
  default = false
}