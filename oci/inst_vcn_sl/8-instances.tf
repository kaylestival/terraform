data "oci_identity_availability_domain" "ad" {
  compartment_id = var.tenancy_ocid
  ad_number      = var.availability_domain_number
}

data "oci_core_images" "freeimage" {
  compartment_id           = var.compartment_compute_id
  operating_system         = var.image1_instance_so
  operating_system_version = var.image1_instance_so_version
  shape                    = var.image1_instance_shape
}

resource "oci_core_instance" "vmfinanceiro" {
  #Necessary
  depends_on = [
    oci_core_subnet.subnetfin
  ]
  #Required
  availability_domain = data.oci_identity_availability_domain.ad.name
  compartment_id      = var.compartment_compute_id
  shape               = var.instance1_shape
  #Optional
  create_vnic_details {
    #Optional
    freeform_tags             = var.general_tags
    subnet_id                 = oci_core_subnet.subnetfin.id
    assign_private_dns_record = var.instance1_create_vnic_details_assign_private_dns_record
    assign_public_ip          = var.instance1_create_vnic_details_assign_public_ip
  }
  display_name                        = var.instance1_display_name
  fault_domain                        = var.instance1_fault_domain
  freeform_tags                       = var.general_tags
  is_pv_encryption_in_transit_enabled = var.instance1_is_pv_encryption_in_transit_enabled
  metadata = {
    ssh_authorized_keys = "${file(var.instance1_ssh_public_key_file)}"
  }
  shape_config {
    #Optional
    memory_in_gbs = var.instance1_shape_config_memory_in_gbs
    ocpus         = var.instance1_shape_config_ocpus
  }
  source_details {
    #Required
    source_id   = data.oci_core_images.freeimage.images.0.id
    source_type = var.instance1_source_type
    #Optional
    boot_volume_size_in_gbs = "100"
    #        kms_key_id = oci_kms_key.test_key.id
  }
  preserve_boot_volume = false
}

resource "oci_core_instance" "vmcomercial" {
  #Necessary
  depends_on = [
    oci_core_subnet.subnetcom
  ]
  #Required
  availability_domain = data.oci_identity_availability_domain.ad.name
  compartment_id      = var.compartment_compute_id
  shape               = var.instance2_shape
  #Optional
  create_vnic_details {
    #Optional
    freeform_tags             = var.general_tags
    subnet_id                 = oci_core_subnet.subnetcom.id
    assign_private_dns_record = var.instance2_create_vnic_details_assign_private_dns_record
    assign_public_ip          = var.instance2_create_vnic_details_assign_public_ip
  }
  display_name                        = var.instance2_display_name
  fault_domain                        = var.instance2_fault_domain
  freeform_tags                       = var.general_tags
  is_pv_encryption_in_transit_enabled = var.instance2_is_pv_encryption_in_transit_enabled
  metadata = {
    ssh_authorized_keys = "${file(var.instance2_ssh_public_key_file)}"
  }
  shape_config {
    #Optional
    memory_in_gbs = var.instance2_shape_config_memory_in_gbs
    ocpus         = var.instance2_shape_config_ocpus
  }
  source_details {
    #Required
    source_id   = data.oci_core_images.freeimage.images.0.id
    source_type = var.instance2_source_type
    #Optional
    boot_volume_size_in_gbs = var.instance2_boot_volume_size
  }
  preserve_boot_volume = false
}