resource "oci_core_vcn" "vcn_mod3" {
  # Required
  compartment_id = var.compartment_network_id
  # Optional
  cidr_blocks   = var.vcn_cidr_blocks
  display_name  = var.vcn_display_name
  dns_label     = var.vcn_dns_label
  freeform_tags = var.general_tags
}