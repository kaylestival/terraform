terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.67.0"
    }

    postgresql = {
      source  = "cyrilgdn/postgresql"
      version = "=1.13.0"
    }


  }
}

provider "azurerm" {
  features {}   
}