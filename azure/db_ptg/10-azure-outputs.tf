output "postgresql_address" {
    value       = "${azurerm_postgresql_server.postgresqlserver.name}.postgres.database.azure.com"
}
