resource "azurerm_subnet" "subnet" {
    name                 = "Subnet"
    resource_group_name  = azurerm_resource_group.resourcegroup.name
    virtual_network_name = azurerm_virtual_network.virtualnetwork.name
    address_prefixes       = ["10.0.2.0/24"]
}
