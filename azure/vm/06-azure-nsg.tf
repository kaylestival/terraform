resource "azurerm_network_security_group" "nsg" {
    name                = "NetworkSecurityGroup"
    location            = "eastus"
    resource_group_name = azurerm_resource_group.resourcegroup.name

    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    tags = {
        ProvisionedBy = "Terraform"
    }
}
