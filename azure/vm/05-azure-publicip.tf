resource "azurerm_public_ip" "publicip" {
    name                         = "PublicIP"
    location                     = "eastus"
    resource_group_name          = azurerm_resource_group.resourcegroup.name
    allocation_method            = "Static"

    tags = {
        ProvisionedBy = "Terraform"
    }
}
