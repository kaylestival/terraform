resource "azurerm_public_ip" "bastionhostpublicip" {
  name                = "Bastion_Host_Public_IP"
  location            = azurerm_resource_group.recursos1.location
  resource_group_name = azurerm_resource_group.recursos1.name
  allocation_method   = "Static"
  sku                 = "Standard"

  tags = {
    ProvisionedBy = "Terraform"
  }

  depends_on = [
    azurerm_resource_group.recursos1
  ]

}

resource "azurerm_bastion_host" "bastionhost" {
  name                = "Bastion_Host"
  location            = azurerm_resource_group.recursos1.location
  resource_group_name = azurerm_resource_group.recursos1.name

  ip_configuration {
    name                 = "Bastion_IP_config"
    subnet_id            = azurerm_subnet.subnetbastionhost.id
    public_ip_address_id = azurerm_public_ip.bastionhostpublicip.id
  }
}