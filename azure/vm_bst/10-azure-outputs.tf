output "network_interface_private_ip_linux" {
  description = "private ip addresses of the vm nics - Linux"
  value       = azurerm_network_interface.niclinux.private_ip_address
}

output "network_interface_private_ip_windows" {
  description = "private ip addresses of the vm nics - Windows"
  value       = azurerm_network_interface.nicwindows.private_ip_address
}

output "public_ip_bastion_host" {
  description = "public ip addresses of the bastion host"
  value       = azurerm_bastion_host.bastionhost.dns_name
}



