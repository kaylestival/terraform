resource "azurerm_subnet" "subnetinterna" {
  name                 = "Subnet_Interna"
  resource_group_name  = azurerm_resource_group.recursos1.name
  virtual_network_name = azurerm_virtual_network.virtualnetwork.name
  address_prefixes     = ["10.6.2.0/24"]

  depends_on = [
    azurerm_resource_group.recursos1,
    azurerm_virtual_network.virtualnetwork
  ]

}


resource "azurerm_subnet" "subnetbastionhost" {
  name                 = "AzureBastionSubnet"
  resource_group_name  = azurerm_resource_group.recursos1.name
  virtual_network_name = azurerm_virtual_network.virtualnetwork.name
  address_prefixes     = ["10.6.1.0/27"]

  depends_on = [
    azurerm_resource_group.recursos1,
    azurerm_virtual_network.virtualnetwork
  ]

}