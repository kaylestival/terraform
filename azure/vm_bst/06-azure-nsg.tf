resource "azurerm_network_security_group" "nsginterna" {
  name                = "NSG_Interna"
  location            = "eastus"
  resource_group_name = azurerm_resource_group.recursos1.name

  security_rule {
    name                         = "NDP"
    priority                     = 201
    direction                    = "Inbound"
    access                       = "Allow"
    protocol                     = "Tcp"
    source_port_range            = "*"
    destination_port_range       = "3389"
    source_address_prefixes      = azurerm_subnet.subnetbastionhost.address_prefixes
    destination_address_prefixes = azurerm_subnet.subnetinterna.address_prefixes
  }

  security_rule {
    name                         = "SSH"
    priority                     = 202
    direction                    = "Inbound"
    access                       = "Allow"
    protocol                     = "Tcp"
    source_port_range            = "*"
    destination_port_range       = "22"
    source_address_prefixes      = azurerm_subnet.subnetbastionhost.address_prefixes
    destination_address_prefixes = azurerm_subnet.subnetinterna.address_prefixes
  }

  tags = {
    ProvisionedBy = "Terraform"
  }

  depends_on = [
    azurerm_resource_group.recursos1,
    azurerm_subnet.subnetinterna
  ]

}