resource "azurerm_resource_group" "recursos1" {
  name     = "Bastion_Host"
  location = "eastus"

  tags = {
    ProvisionedBy = "Terraform"
  }
}