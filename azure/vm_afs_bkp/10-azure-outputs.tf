output "public_ip_address_vmlinux01" {
    value       = azurerm_public_ip.publicip1.ip_address
}


output "public_ip_address_vmlinux02" {
    value       = azurerm_public_ip.publicip2.ip_address
}
