resource "azurerm_subnet" "subnet" {
    name                 = "Subnet"
    resource_group_name  = azurerm_resource_group.recursos1.name
    virtual_network_name = azurerm_virtual_network.virtualnetwork.name
    address_prefixes       = ["10.3.1.0/24"]
}