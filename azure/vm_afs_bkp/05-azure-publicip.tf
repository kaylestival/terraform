resource "azurerm_public_ip" "publicip1" {
    name                         = "PublicIP1"
    location                     = "eastus"
    resource_group_name          = azurerm_resource_group.recursos1.name
    allocation_method            = "Static"

    tags = {
        ProvisionedBy = "Terraform"
    }
}



resource "azurerm_public_ip" "publicip2" {
    name                         = "PublicIP2"
    location                     = "eastus"
    resource_group_name          = azurerm_resource_group.recursos1.name
    allocation_method            = "Static"

    tags = {
        ProvisionedBy = "Terraform"
    }
}