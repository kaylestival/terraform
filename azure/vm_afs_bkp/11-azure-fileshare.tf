resource "azurerm_storage_share" "storageshare" {
  name                 = "app-shared"
  storage_account_name = azurerm_storage_account.storageaccount.name
  quota                = 1
}