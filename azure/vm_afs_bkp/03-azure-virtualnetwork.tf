resource "azurerm_virtual_network" "virtualnetwork" {
    name                = "Vnet"
    address_space       = ["10.3.0.0/16"]
    location            = "eastus"
    resource_group_name = azurerm_resource_group.recursos1.name

    tags = {
        ProvisionedBy = "Terraform"
    }
}