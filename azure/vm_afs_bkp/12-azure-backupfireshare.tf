resource "azurerm_recovery_services_vault" "recoveryservicesvault" {
  name                = "RecoveryServicesVault"
  location            = azurerm_resource_group.recursos1.location
  resource_group_name = azurerm_resource_group.recursos1.name
  sku                 = "Standard"
}

resource "azurerm_backup_policy_file_share" "policyfileshare" {
  name                = "BackupDiarioapp-shared"
  resource_group_name = azurerm_resource_group.recursos1.name
  recovery_vault_name = azurerm_recovery_services_vault.recoveryservicesvault.name

  timezone = "UTC"

  backup {
    frequency = "Daily"
    time      = "01:30"
  }

  retention_daily {
    count = 30
  }

}


resource "azurerm_backup_container_storage_account" "containerstorageaccount" {
  resource_group_name = azurerm_resource_group.recursos1.name
  recovery_vault_name = azurerm_recovery_services_vault.recoveryservicesvault.name
  storage_account_id  = azurerm_storage_account.storageaccount.id
}


resource "azurerm_backup_protected_file_share" "protectedfileshare" {
  resource_group_name       = azurerm_resource_group.recursos1.name
  recovery_vault_name       = azurerm_recovery_services_vault.recoveryservicesvault.name
  source_storage_account_id = azurerm_backup_container_storage_account.containerstorageaccount.storage_account_id
  source_file_share_name    = azurerm_storage_share.storageshare.name
  backup_policy_id          = azurerm_backup_policy_file_share.policyfileshare.id
}