resource "azurerm_linux_virtual_machine" "vmlinux01" {
    name                  = "appvm1"
    location              = "eastus"
    resource_group_name   = azurerm_resource_group.recursos1.name
    network_interface_ids = [azurerm_network_interface.niclinux1.id]
    size                  = "Standard_B1s"

    os_disk {
        name              = "myOsDisk1"
        caching           = "ReadWrite"
        storage_account_type = "Premium_LRS"
    }

    source_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "18.04-LTS"
        version   = "latest"
    }

    computer_name  = "appvm1"
    admin_username = "azureuser"
    disable_password_authentication = true

    admin_ssh_key {
        username       = "azureuser"
        public_key     = file("./id_rsa.pub")
    }

    boot_diagnostics {
        storage_account_uri = azurerm_storage_account.storageaccount.primary_blob_endpoint
    }

  connection {
   host        = azurerm_public_ip.publicip1.ip_address
   agent       = false
   type        = "ssh"
   user        = "azureuser"
   private_key = file(pathexpand("./id_rsa"))
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mkdir -p /mnt/${azurerm_storage_share.storageshare.name}",
      "sudo mount -t cifs //${azurerm_storage_account.storageaccount.name}.file.core.windows.net/${azurerm_storage_share.storageshare.name} /mnt/${azurerm_storage_share.storageshare.name} -o vers=3.0,username=${azurerm_storage_account.storageaccount.name},password=${azurerm_storage_account.storageaccount.primary_access_key},dir_mode=0777,file_mode=0777,serverino",
#      "sudo echo \"//${azurerm_storage_account.storageaccount.name}.file.core.windows.net/${azurerm_storage_share.storageshare.name} /mnt/${azurerm_storage_share.storageshare.name} cifs nofail,vers=3.0,username=${azurerm_storage_account.storageaccount.name},password=${azurerm_storage_account.storageaccount.primary_access_key},dir_mode=0777,file_mode=0777,serverino\" >> /etc/fstab"
    ]
  }


    tags = {
        ProvisionedBy = "Terraform"
    }

    depends_on = [
        azurerm_storage_share.storageshare
    ]

}



resource "azurerm_linux_virtual_machine" "vmlinux02" {
    name                  = "appvm2"
    location              = "eastus"
    resource_group_name   = azurerm_resource_group.recursos1.name
    network_interface_ids = [azurerm_network_interface.niclinux2.id]
    size                  = "Standard_B1s"

    os_disk {
        name              = "myOsDisk2"
        caching           = "ReadWrite"
        storage_account_type = "Premium_LRS"
    }

    source_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "18.04-LTS"
        version   = "latest"
    }

    computer_name  = "appvm2"
    admin_username = "azureuser"
    disable_password_authentication = true

    admin_ssh_key {
        username       = "azureuser"
        public_key     = file("./id_rsa.pub")
    }

    boot_diagnostics {
        storage_account_uri = azurerm_storage_account.storageaccount.primary_blob_endpoint
    }

  connection {
   host        = azurerm_public_ip.publicip2.ip_address
   agent       = false
   type        = "ssh"
   user        = "azureuser"
   private_key = file(pathexpand("./id_rsa"))
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mkdir -p /mnt/${azurerm_storage_share.storageshare.name}",
      "sudo mount -t cifs //${azurerm_storage_account.storageaccount.name}.file.core.windows.net/${azurerm_storage_share.storageshare.name} /mnt/${azurerm_storage_share.storageshare.name} -o vers=3.0,username=${azurerm_storage_account.storageaccount.name},password=${azurerm_storage_account.storageaccount.primary_access_key},dir_mode=0777,file_mode=0777,serverino",
#      "sudo echo \"//${azurerm_storage_account.storageaccount.name}.file.core.windows.net/${azurerm_storage_share.storageshare.name} /mnt/${azurerm_storage_share.storageshare.name} cifs nofail,vers=3.0,username=${azurerm_storage_account.storageaccount.name},password=${azurerm_storage_account.storageaccount.primary_access_key},dir_mode=0777,file_mode=0777,serverino\" >> /etc/fstab"
    ]
  }


    tags = {
        ProvisionedBy = "Terraform"
    }

    depends_on = [
        azurerm_storage_share.storageshare
    ]

}