resource "azurerm_network_interface" "niclinux1" {
    name                        = "NIC1"
    location                    = "eastus"
    resource_group_name         = azurerm_resource_group.recursos1.name

    ip_configuration {
        name                          = "NicConfiguratio1"
        subnet_id                     = azurerm_subnet.subnet.id
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = azurerm_public_ip.publicip1.id
    }

    tags = {
        ProvisionedBy = "Terraform"
    }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "default1" {
    network_interface_id      = azurerm_network_interface.niclinux1.id
    network_security_group_id = azurerm_network_security_group.nsg.id
}



resource "azurerm_network_interface" "niclinux2" {
    name                        = "NIC2"
    location                    = "eastus"
    resource_group_name         = azurerm_resource_group.recursos1.name

    ip_configuration {
        name                          = "NicConfiguratio2"
        subnet_id                     = azurerm_subnet.subnet.id
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = azurerm_public_ip.publicip2.id
    }

    tags = {
        ProvisionedBy = "Terraform"
    }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "default2" {
    network_interface_id      = azurerm_network_interface.niclinux2.id
    network_security_group_id = azurerm_network_security_group.nsg.id
}