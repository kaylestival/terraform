resource "azurerm_network_security_group" "nsg" {
    name                = "NetworkSecurityGroup"
    location            = "eastus"
    resource_group_name = azurerm_resource_group.resourcegroup.name

    tags = {
        ProvisionedBy = "Terraform"
    }
}