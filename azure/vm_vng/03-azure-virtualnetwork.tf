resource "azurerm_virtual_network" "virtualnetwork" {
    name                = "Vnet"
    address_space       = ["10.0.0.0/16"]
    location            = "eastus"
    resource_group_name = azurerm_resource_group.resourcegroup.name

    tags = {
        ProvisionedBy = "Terraform"
    }
}