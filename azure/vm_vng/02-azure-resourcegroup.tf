resource "azurerm_resource_group" "resourcegroup" {
    name     = "Resource_Group"
    location = "eastus"

    tags = {
        ProvisionedBy = "Terraform"
    }
}