resource "azurerm_public_ip" "publicip" {
  name                = "PublicIP"
  location            = azurerm_resource_group.resourcegroup.location
  resource_group_name = azurerm_resource_group.resourcegroup.name
  allocation_method = "Dynamic"
}


resource "azurerm_subnet" "gatewaysubnet" {
    name                 = "GatewaySubnet"
    resource_group_name  = azurerm_resource_group.resourcegroup.name
    virtual_network_name = azurerm_virtual_network.virtualnetwork.name
    address_prefixes       = ["10.0.2.0/24"]
}


resource "azurerm_virtual_network_gateway" "vng" {
  name                = "VNG"
  location            = azurerm_resource_group.resourcegroup.location
  resource_group_name = azurerm_resource_group.resourcegroup.name

  type     = "Vpn"
  vpn_type = "RouteBased"

  active_active = false
  enable_bgp    = false
  sku           = "VpnGw1"
  generation    = "Generation1"

  ip_configuration {
    name                          = "vnetGatewayConfig"
    public_ip_address_id          = azurerm_public_ip.publicip.id
    private_ip_address_allocation = "Dynamic"
    subnet_id                     = azurerm_subnet.gatewaysubnet.id
  }

  vpn_client_configuration {
    address_space = ["172.16.0.0/24"]
    vpn_client_protocols = ["SSTP", "IkeV2"]

    root_certificate {
      name = "P2SRootCert"

      public_cert_data = <<EOF
<PUBLIC_KEY_CONTENTS>
EOF

    }
  }

    tags = {
        ProvisionedBy = "Terraform"
    }

}