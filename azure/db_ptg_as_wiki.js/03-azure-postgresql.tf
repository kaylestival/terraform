resource "azurerm_postgresql_server" "postgresqlserver" {
  name                = "us-db01"
  location            = azurerm_resource_group.recursos1.location
  resource_group_name = azurerm_resource_group.recursos1.name

  administrator_login          = "sql"
  administrator_login_password = "H@Sh1CoR3!"

  sku_name   = "B_Gen5_2"
  version    = "10"
  storage_mb = 51200

  backup_retention_days        = 7
  geo_redundant_backup_enabled = false
  auto_grow_enabled            = true

  public_network_access_enabled = true
  ssl_enforcement_enabled       = true

  tags = {
    ProvisionedBy = "Terraform"
  }

}

resource "azurerm_postgresql_firewall_rule" "postgresql-fw-rule" {
  name                = "PostgreSQLAccess"
  resource_group_name = azurerm_resource_group.recursos1.name
  server_name         = azurerm_postgresql_server.postgresqlserver.name
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "0.0.0.0"
}

resource "azurerm_postgresql_database" "dbpostgresql" {
  name                = "db01"
  resource_group_name = azurerm_resource_group.recursos1.name
  server_name         = azurerm_postgresql_server.postgresqlserver.name
  charset             = "UTF8"
  collation           = "English_United States.1252"

  depends_on = [
    azurerm_postgresql_firewall_rule.postgresql-fw-rule,
  ]

}