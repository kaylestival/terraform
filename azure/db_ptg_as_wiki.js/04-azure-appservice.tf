resource "azurerm_app_service_plan" "appserviceplan" {
  name                = "app-service-plan"
  location            = azurerm_resource_group.recursos1.location
  resource_group_name = azurerm_resource_group.recursos1.name
  kind                = "Linux"
  reserved            = true

  sku {
    tier = "Basic"
    size = "B1"
  }
}

resource "azurerm_app_service" "appservice" {
  name                = "ksvwiki"
  location            = azurerm_resource_group.recursos1.location
  resource_group_name = azurerm_resource_group.recursos1.name
  app_service_plan_id = azurerm_app_service_plan.appserviceplan.id

  site_config {
    linux_fx_version = "DOCKER|requarks/wiki:latest"
  }

  app_settings = {
    "WEBSITES_ENABLE_APP_SERVICE_STORAGE" = "false"
    "DB_HOST" = "${azurerm_postgresql_server.postgresqlserver.name}.postgres.database.azure.com"
    "DB_NAME" = "${azurerm_postgresql_database.dbpostgresql.name}"
    "DB_PASS" = "${azurerm_postgresql_server.postgresqlserver.administrator_login_password}"
    "DB_PORT" = "5432"
    "DB_SSL"  = "true"
    "DB_TYPE" = "postgres"
    "DB_USER" = "${azurerm_postgresql_server.postgresqlserver.administrator_login}@${azurerm_postgresql_server.postgresqlserver.name}"
  }

  depends_on = [
    azurerm_postgresql_database.dbpostgresql,
    azurerm_postgresql_firewall_rule.postgresql-fw-rule1,
    azurerm_postgresql_firewall_rule.postgresql-fw-rule2,
  ]

}