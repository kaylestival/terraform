output "network_interface_private_ip_linux" {
  description = "private ip addresses of the vm nics - Linux"
  value       = azurerm_network_interface.niclinux.private_ip_address
}

output "network_interface_private_ip_windows" {
  description = "private ip addresses of the vm nics - Windows"
  value       = azurerm_network_interface.nicwindows.private_ip_address
}


output "private_ip_address_firewall" {
  description = "private ip addresses of the firewall"
  value       = azurerm_firewall.desafiofire.ip_configuration[0].private_ip_address
}