resource "azurerm_windows_virtual_machine" "vmwindows" {
  name                = "VM_Windows"
  resource_group_name = azurerm_resource_group.recursos1.name
  location            = azurerm_resource_group.recursos1.location
  size                = "Standard_B1s"
  admin_username      = "adminuser"
  admin_password      = "P@$$w0rd1234!"
  network_interface_ids = [
    azurerm_network_interface.nicwindows.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsDesktop"
    offer     = "windows-10"
    sku       = "19h2-pro-g2"
    version   = "latest"
  }

  computer_name  = "VMWindows01"
  enable_automatic_updates = true
  provision_vm_agent       = false
  allow_extension_operations = false

    tags = {
        ProvisionedBy = "Terraform"
    }

  depends_on = [
    azurerm_resource_group.recursos1,
    azurerm_network_interface.nicwindows
  ]

}