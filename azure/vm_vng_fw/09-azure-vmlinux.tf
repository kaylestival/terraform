resource "azurerm_linux_virtual_machine" "vmlinux" {
    name                  = "VM_Linux"
    location              = "eastus"
    resource_group_name   = azurerm_resource_group.recursos1.name
    network_interface_ids = [azurerm_network_interface.niclinux.id]
    size                  = "Standard_B1s"

    os_disk {
        name              = "MyOsDisk"
        caching           = "ReadWrite"
        storage_account_type = "Premium_LRS"
    }

    source_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "18.04-LTS"
        version   = "latest"
    }

    computer_name  = "VMLinux01"
    admin_username = "azureuser"
    disable_password_authentication = true
    allow_extension_operations = false

    admin_ssh_key {
        username       = "azureuser"
        public_key     = file("./id_rsa.pub")
    }

    boot_diagnostics {
        storage_account_uri = azurerm_storage_account.storageaccount.primary_blob_endpoint
    }

    tags = {
        ProvisionedBy = "Terraform"
    }

  depends_on = [
    azurerm_resource_group.recursos1,
    azurerm_network_interface.niclinux,
    azurerm_storage_account.storageaccount
  ]

}