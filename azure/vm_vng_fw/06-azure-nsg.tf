resource "azurerm_network_security_group" "nsg" {
    name                = "NetworkSecurityGroup"
    location            = "eastus"
    resource_group_name = azurerm_resource_group.recursos1.name

    tags = {
        ProvisionedBy = "Terraform"
    }

  depends_on = [
    azurerm_resource_group.recursos1,
    azurerm_subnet.subnet
  ]

}