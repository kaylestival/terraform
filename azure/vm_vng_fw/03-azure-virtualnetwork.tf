resource "azurerm_virtual_network" "virtualnetwork" {
    name                = "VnetBootcamp"
    address_space       = ["10.2.0.0/16"]
    location            = azurerm_resource_group.recursos1.location
    resource_group_name = azurerm_resource_group.recursos1.name

    tags = {
        ProvisionedBy = "Terraform"
    }

  depends_on = [
    azurerm_resource_group.recursos1
  ]

}