resource "azurerm_network_interface" "niclinux" {
    name                        = "NIC_Linux"
    location                    = "eastus"
    resource_group_name         = azurerm_resource_group.recursos1.name

    ip_configuration {
        name                          = "NicConfiguration"
        subnet_id                     = azurerm_subnet.subnet.id
        private_ip_address_allocation = "Dynamic"
    }

    tags = {
        ProvisionedBy = "Terraform"
    }

    depends_on = [
        azurerm_resource_group.recursos1,
        azurerm_subnet.subnet
  ]

}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "defaultlinux" {
    network_interface_id      = azurerm_network_interface.niclinux.id
    network_security_group_id = azurerm_network_security_group.nsg.id

  depends_on = [
    azurerm_network_interface.niclinux,
    azurerm_network_security_group.nsg
  ]

}



resource "azurerm_network_interface" "nicwindows" {
  name                = "NIC_Windows"
  location            = azurerm_resource_group.recursos1.location
  resource_group_name = azurerm_resource_group.recursos1.name

  ip_configuration {
    name                          = "NicConfigurationWindows"
    subnet_id                     = azurerm_subnet.subnet.id
    private_ip_address_allocation = "Dynamic"
  }

    tags = {
        ProvisionedBy = "Terraform"
    }

  depends_on = [
    azurerm_resource_group.recursos1,
    azurerm_subnet.subnet
  ]

}


# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "defaultwindows" {
    network_interface_id      = azurerm_network_interface.nicwindows.id
    network_security_group_id = azurerm_network_security_group.nsg.id

  depends_on = [
    azurerm_network_interface.nicwindows,
    azurerm_network_security_group.nsg
  ]

}