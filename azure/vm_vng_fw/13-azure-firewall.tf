resource "azurerm_subnet" "azurefirewallsubnet" {
    name                 = "AzureFirewallSubnet"
    resource_group_name  = azurerm_resource_group.recursos1.name
    virtual_network_name = azurerm_virtual_network.virtualnetwork.name
    address_prefixes       = ["10.2.3.0/26"]

  depends_on = [
    azurerm_resource_group.recursos1,
    azurerm_virtual_network.virtualnetwork
  ]

}

resource "azurerm_public_ip" "azurefirewallpublicip" {
  name                = "AzureFirewallPublicIP"
  location            = azurerm_resource_group.recursos1.location
  resource_group_name = azurerm_resource_group.recursos1.name
  allocation_method = "Static"
  sku = "Standard"

    tags = {
        ProvisionedBy = "Terraform"
    }

  depends_on = [
    azurerm_resource_group.recursos1
  ]

}


resource "azurerm_firewall" "desafiofire" {
  name                = "DesafioFire"
  location            = azurerm_resource_group.recursos1.location
  resource_group_name = azurerm_resource_group.recursos1.name

  ip_configuration {
    name                 = "configuration"
    subnet_id            = azurerm_subnet.azurefirewallsubnet.id
    public_ip_address_id = azurerm_public_ip.azurefirewallpublicip.id

  }

    tags = {
        ProvisionedBy = "Terraform"
    }

  depends_on = [
    azurerm_public_ip.azurefirewallpublicip,
    azurerm_network_interface.niclinux,
    azurerm_network_interface.nicwindows,
    azurerm_virtual_network_gateway.vpnbootcamp
  ]

}


resource "azurerm_firewall_network_rule_collection" "dnsrule" {
  name = "azure-firewall-dns-rule"
  azure_firewall_name = azurerm_firewall.desafiofire.name
  resource_group_name = azurerm_resource_group.recursos1.name
  priority = 100
  action = "Allow"
  rule {
    name = "DNS"   
    source_addresses = ["10.2.0.0/16"]    
    destination_ports = ["53"]
    destination_addresses = ["8.8.8.8","8.8.4.4"]
    protocols = ["TCP","UDP"]
  }
}


resource "azurerm_firewall_application_rule_collection" "apprule" {
  name                = "AppRule"
  azure_firewall_name = azurerm_firewall.desafiofire.name
  resource_group_name = azurerm_resource_group.recursos1.name
  priority            = 200
  action              = "Allow"

  rule {
    name = "liberafacebook"
    source_addresses = ["10.2.0.0/16"]
    target_fqdns = ["*.facebook.com"]
    protocol {
      port = "443"
      type = "Https"
    }
  }

  rule {
    name = "liberainstagram"
    source_addresses = ["10.2.0.0/16"]
    target_fqdns = ["*.instagram.com"]
    protocol {
      port = "443"
      type = "Https"
    }
  }

}


#Create Routinf Table for internal subnet
resource "azurerm_route_table" "routetable" {
  name                          = "RouteTable"
  resource_group_name           = azurerm_resource_group.recursos1.name
  location                      = azurerm_resource_group.recursos1.location
  disable_bgp_route_propagation = false

  route {
    name                    = "Default"
    address_prefix          = "0.0.0.0/0"
    next_hop_type           = "VirtualAppliance"
    next_hop_in_ip_address  = azurerm_firewall.desafiofire.ip_configuration[0].private_ip_address
  } 
}

#Assign RT to Internal Subnet
resource "azurerm_subnet_route_table_association" "routetableassociation" {
  subnet_id      = azurerm_subnet.subnet.id
  route_table_id = azurerm_route_table.routetable.id
}