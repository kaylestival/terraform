# **List of resource creation files**

**Cloud provider**: Azure 

- **vm directory**: Virtual Machine Linux, including network structure, public IP, storage account and using an asymmetric key pair previasly created.
- **vm_afs_bkp directory**: Virtual Machines Linux, Azure File Storage and Azure Backup, mapping file share in VM's and using an asymmetric key pair previasly created.
- **vm_vng directory**: Virtual Machine Linux and Virtual Network Gateway, including network structure, storage account and using an asymmetric key pair previasly created.
- **vm_vng_fw directory**: Virtual Machine Linux and Windows, Virtual Network Gateway and Azure Firewall, including network structure, storage account and using an asymmetric key pair previasly created.
- **vm_bst**: Virtual Machine Linux and Windows, Azure Bastion, including network structure, public IP, storage account and using an asymmetric key pair previasly created.
- **db_ptg directory**: Server and database PostgreSQL, including schema and IP to allow access to the base.
- **db_ptg_as_wiki.js directory**: Server and database PostgreSQL, and Azure App Service with Wiki.js's docker container.

More, soon.
